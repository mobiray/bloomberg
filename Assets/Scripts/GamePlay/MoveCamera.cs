﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MoveCamera : MonoBehaviour
{


    public GameInfoScript gameInfoScript;
    public GameObject linePrefab;
    public GameObject gameOverPanel;
    public Material red;
    public Material green;
    public float startSum;
    public float endSum;
    public float totalSum = 0;
    private float coefficient;
    private float currentAmountLocal;
    public float startAmount;

    bool flag = false;
    public Text amountText;
    public Text counterText;
    Tweener t;
    Camera cam;
    bool isGameOver = false;
    LineChanger lc;
    float height;
    float width;
    LineRenderer lr;
    List<GameObject> g;
    private bool checkedSum = false;

    public float CurrentAmount
    {
        get
        {
            return currentAmountLocal;
        }

        set
        {
            currentAmountLocal = value;
        }
    }

    public float Coefficient
    {
        get
        {
            return coefficient;
        }

        set
        {
            coefficient = value;
        }
    }

    // Use this for initialization
    void OnEnable()
    {
        checkedSum = false;
        g = new List<GameObject>();
        currentAmountLocal = gameInfoScript.MaxPrice;
        startAmount = currentAmountLocal;
        isGameOver = false;
        lc = GameObject.Find("Diagramm").GetComponent<LineChanger>();
        cam = Camera.main;
        cam.GetComponent<Camera>().orthographicSize = 2.71f;
        height = 2f * cam.orthographicSize;
        width = height * cam.aspect;
        cam.transform.position = new Vector3(-width / 2, lc.Height[0], -10f);
        Vector3[] path = new Vector3[lc.Height.Length];
        for (int i = 0; i < 5; i++)
        {
            path[i] = new Vector3(-width / 2, lc.Height[0], -10);
        }
        for (int i = 5; i < path.Length; i++)
        {
            if (Mathf.Abs(path[i - 1].y - lc.Height[i]) > height / 4)
            {
                path[i] = new Vector3(i * 0.1f - width / 2, lc.Height[i], -10);
            }
            else
            {
                path[i] = new Vector3(i * 0.1f - width / 2, lc.Height[i] - lc.Height[i - 5] - height / 20 < 0 ? path[i - 1].y : lc.Height[i], -10);
            }
        }
        t = cam.transform.DOPath(path, 30f, pathMode: PathMode.Sidescroller2D).SetEase(Ease.Linear);

    }

    private void OnDisable()
    {
        t.Kill();
    }

    public int GetStep()
    {
        int step;
        step = Mathf.RoundToInt((cam.transform.position.x + width / 2) / .1f);
        return step;
    }

    private void Update()
    {
        if (!isGameOver)
        {
            counterText.text = Mathf.RoundToInt((1 - t.ElapsedPercentage()) * t.Duration()) + "s";
            if (Input.GetMouseButtonDown(0) && !flag)
            {
                flag = true;
                OnPointDown();
            }
            if (Input.GetMouseButtonUp(0) && flag)
            {
                OnPointUp();
                flag = false;
            }
            if (flag)
            {
                endSum = lc.Height[GetStep()];
                amountText.text = Mathf.FloorToInt(currentAmountLocal + (endSum - startSum) * currentAmountLocal / lc.MAX_PRICE) + "$";
                g[g.Count - 1].GetComponent<LineRenderer>().SetPosition(1, new Vector3(cam.transform.position.x + width / 2, lc.Height[GetStep()], -1));
                if (startSum > endSum)
                    g[g.Count - 1].GetComponent<LineRenderer>().material = red;
                else
                    g[g.Count - 1].GetComponent<LineRenderer>().material = green;
            }
            if (!t.IsPlaying())
            {
                if (flag)
                {
                    OnPointUp();
                    flag = false;
                }
                cam.GetComponent<Camera>().orthographicSize = 15.5f;
                cam.transform.position = new Vector3(25, 15.5f, -10);
                isGameOver = true;
            }
        }
        else
        {
            counterText.text = "";
            amountText.text = "";
            if (!checkedSum)
            {
                gameInfoScript.CurrentAmount = gameInfoScript.CurrentAmount - gameInfoScript.MaxPrice + currentAmountLocal;
                gameInfoScript.SaveAmount();
                checkedSum = true;
            }
            foreach (GameObject go in g)
            {
                Destroy(go);
            }
            g.Clear();
            gameOverPanel.SetActive(true);
            gameOverPanel.GetComponentInChildren<Text>().text = "You got: " + (currentAmountLocal - startAmount) + "$";
        }
    }


    public void OnPointDown()
    {
        startSum = lc.Height[GetStep()];
        g.Add(Instantiate(linePrefab));
        g[g.Count - 1].transform.SetParent(lc.transform);
        g[g.Count - 1].GetComponent<LineRenderer>().SetPosition(0, new Vector3(cam.transform.position.x + width / 2, lc.Height[GetStep()], -1));

    }

    public void OnPointUp()
    {
        endSum = lc.Height[GetStep()];
        totalSum = (endSum - startSum) * currentAmountLocal / lc.MAX_PRICE;
        currentAmountLocal += totalSum;
        
        gameInfoScript.SaveAmount();
    }
}
