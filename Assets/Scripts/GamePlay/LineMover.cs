﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineMover : MonoBehaviour {

    LineChanger lc;
    float height;
    public float width;
    float startSum;
    float endSum;
    public float totalSum = 0;
    public int currentStep = 1;
    Vector3[] path;

    void OnEnable()
    {
        lc = GameObject.Find("Diagramm").GetComponent<LineChanger>();
        Camera cam = Camera.main;
        height = 2f * cam.orthographicSize;
        width = height * cam.aspect;
        
        path = new Vector3[lc.Height.Length];
        for (int i = 0; i < 5; i++)
            path[i] = new Vector3(width / 2, lc.Height[0],0f);

        transform.position = new Vector3(0, 0, 0f);

        for (int i = 5; i < path.Length; i++)
        {
            if (Mathf.Abs(path[i - 1].y - lc.Height[i]) > height / 4)
            {
                path[i] = new Vector3(-i * 0.1f+  width / 2, lc.Height[i], 0f);
            }
            else
            {
                path[i] = new Vector3(-i * 0.1f + width / 2, lc.Height[i] - lc.Height[i - 5] - height / 20 < 0 ? path[i - 1].y : lc.Height[i], 0f);
            }
        }
    }
}
