﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{
    public  GameObject gis;
    GameObject menuPanel;
    public GameObject gamePlay;
    public GameObject inGamePanel;
    float maxPrice = 0;

    private void Start()
    {
        menuPanel = GameObject.Find("MenuPanel");
    }

    public void OnStartGameButtonClick()
    {
        
        menuPanel.SetActive(false);
        float.TryParse(gameObject.tag, out maxPrice);
        gis.GetComponent<GameInfoScript>().MaxPrice = maxPrice;
        gamePlay.SetActive(true);
        inGamePanel.SetActive(true);
        inGamePanel.GetComponentInChildren<Text>().text = Mathf.FloorToInt(maxPrice) + "$";
        //MoveCamera mc=gamePlay.GetComponentInChildren<MoveCamera>();
        
    }
}
