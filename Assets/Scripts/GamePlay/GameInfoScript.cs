﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfoScript : MonoBehaviour {

    private float currentAmount;

    private float maxPrice;

    public float CurrentAmount
    {
        get
        {
            return currentAmount;
        }

        set
        {
            currentAmount = value;
        }
    }

    public float MaxPrice
    {
        get
        {
            return maxPrice;
        }

        set
        {
            maxPrice = value;
        }
    }

    private void Start()
    {
        if (!PlayerPrefs.HasKey("currentAmount"))
        {
            PlayerPrefs.SetFloat("currentAmount", 500f);
        }

        CurrentAmount = PlayerPrefs.GetFloat("currentAmount");

    }

    private void OnDisable()
    {
        SaveAmount();
    }

    public void SaveAmount()
    {
        PlayerPrefs.SetFloat("currentAmount", CurrentAmount);
    }

}
