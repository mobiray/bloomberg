﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineChanger : MonoBehaviour
{
    public int count ;
    public LineRenderer lr;
    public LineRenderer lrBack;
    public float width = .1f;
    public Vector3[] newVertices;
    public Vector3[] newNormals;
    public Vector2[] newUV;
    public int[] newTriangles;
    private float[] height;
    public float startPrice=100;
    public float coefficient = 100;
    public float MIN_PRICE=3;
    public float MAX_PRICE=20;

    public float[] Height
    {
        get
        {
            return height;
        }

        set
        {
            height = value;
        }
    }

    void OnEnable()
    {
        MeshFilter mf = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        mf.mesh=mesh;
        startPrice = Random.Range(MIN_PRICE, MAX_PRICE);
        height = new float[count];
        height[0] = startPrice;
        for (int i = 1; i < height.Length; i++)
        {
            height[i] = GetNextPrice(height[i-1]);
        }

        //lines behind
        List<LineRenderer> listHorisontalLines=new List<LineRenderer>();
        for (int i = 0; i < Mathf.FloorToInt(MAX_PRICE + 15); i++)
        {
            LineRenderer line=Instantiate(lrBack);
            line.transform.SetParent(transform);
            line.numPositions = 2;
            line.SetPositions(new Vector3[2] { new Vector3(-20f, i, 0f), new Vector3(height.Length * width + 10, i, 0f) });
            listHorisontalLines.Add(line);
        }

        //contour
        Vector3[] linePositions = new Vector3[height.Length * 2];
        for (int i = 0; i < height.Length*2; i+=2)
        {
            linePositions[i] = new Vector3(i * width/2, height[i/2], 0f);
            linePositions[i+1] = new Vector3((i+1.5f)* width/2, height[i/2], 0f);
        }
        lr.numPositions = linePositions.Length;
        lr.SetPositions(linePositions);
        //diagramm
        newVertices = new Vector3[count * 3 + 1];
        newVertices[0] = new Vector3(0, 0, 0);
        for (int i = 1; i < newVertices.Length; i+=3)
        {
            newVertices[i] = new Vector3(newVertices[i - 1].x, height[i/3], 0);
            newVertices[i+1] = new Vector3(newVertices[i - 1].x+width, height[i/3], 0);
            newVertices[i + 2] = new Vector3(newVertices[i - 1].x + width, 0, 0);
        }

        newNormals = new Vector3[newVertices.Length];
        for (int k = 0; k < newNormals.Length; k++)
        {
            newNormals[k] = -Vector3.forward;
        }

        newTriangles = new int[(count+1)*2*3];
        for (int j = 0; j < newTriangles.Length-6; j+=6)
        {
            newTriangles[j] = j - j / 2;
            newTriangles[j + 1] = j + 1 - j / 2;
            newTriangles[j + 2] = j + 3 - j / 2;
            newTriangles[j + 3] = j + 1 - j / 2;
            newTriangles[j + 4] = j + 2 - j / 2;
            newTriangles[j + 5] = j + 3 - j / 2;
        }

        newUV = new Vector2[newVertices.Length];
        for (int j = 0; j < newUV.Length-3; j += 3)
        {
            newUV[j] = new Vector2(0,0);
            newUV[j + 1] = new Vector2(0, 1);
            newUV[j + 2] = new Vector2(1, 0);
            
        }

        mesh.vertices = newVertices;
        mesh.triangles =newTriangles;
        mesh.normals = newNormals;
        mesh.uv = newUV;
    }

    private float GetNextPrice(float oldPrice)
    {
        // Instead of a fixed volatility, pick a random volatility
        // each time, between 2 and 10.
        float volatility = Random.value * 7 + 2;

        float rnd = Random.value;

        float changePercent = 2 * volatility * rnd;

        if (changePercent > volatility)
        {
            changePercent -= (2 * volatility);
        }
        float changeAmount = oldPrice * changePercent / 100;
        float newPrice = oldPrice + changeAmount;

        // Add a ceiling and floor.
        if (newPrice < MIN_PRICE)
        {
            newPrice += Mathf.Abs(changeAmount) * 2;
        }
        else if (newPrice > MAX_PRICE)
        {
            newPrice -= Mathf.Abs(changeAmount) * 2;
        }

        return newPrice;

    }
}