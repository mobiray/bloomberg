﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnablingButtons : MonoBehaviour {

    public GameInfoScript gis;

    private void OnEnable()
    {
        MakeButtonsEnabled();
    }

    public void MakeButtonsEnabled()
    {
        Button[] buttons = GetComponentsInChildren<Button>();
        foreach (Button b in buttons)
        {
            int k = 0;
            int.TryParse(b.tag, out k);
            if (k > gis.CurrentAmount)
            {
                b.interactable = false;
                b.GetComponentInChildren<Text>().color = new Color32(101, 219, 251, 33);
            }
            else
            {
                b.interactable = true;
                b.GetComponentInChildren<Text>().color = new Color32(101, 219, 251, 255);
            }
        }
    }
}
