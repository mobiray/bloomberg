﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSlider : MonoBehaviour {

    float[] buttonValues = { 100,1000,10000,50000,1000000,10000000};
    public GameInfoScript gis;

    private void OnEnable()
    {
        MakeChanges();

    }

    public void MakeChanges()
    {
        float value = 0;
        if (gis.CurrentAmount > buttonValues[5])
        {
            value = 1;
        }
        else if (gis.CurrentAmount > buttonValues[4])
        {
            value = gis.CurrentAmount / buttonValues[5];
        }
        else if (gis.CurrentAmount > buttonValues[3])
        {
            value = gis.CurrentAmount / buttonValues[4];
        }
        else if (gis.CurrentAmount > buttonValues[2])
        {
            value = gis.CurrentAmount / buttonValues[3];
        }
        else if (gis.CurrentAmount > buttonValues[1])
        {
            value = gis.CurrentAmount / buttonValues[2];
        }
        else if (gis.CurrentAmount > buttonValues[0])
        {
            value = gis.CurrentAmount / buttonValues[1];
        }

        GetComponent<Slider>().value = value;
        GetComponentInChildren<Text>().text = Mathf.FloorToInt(gis.CurrentAmount) + "$";
    }
}
