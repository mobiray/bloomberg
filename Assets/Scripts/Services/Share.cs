﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class Share : MonoBehaviour
{
    public GameInfoScript gis;
    private bool isProcessing = false;

    private string shareText = "123";
    private string gameLink = " Download the game on play store at " + "\nhttps://goo.gl/j73gLp";
    private string subject = "Bloomberg game";

    public void ShareText()
    {

        if (!isProcessing)
            StartCoroutine(ShareCO());

    }

    private IEnumerator ShareCO()
    {
        yield return new WaitForEndOfFrame();
        isProcessing = true;
        shareText ="I have earned :"+gis.CurrentAmount+"$ in Bloomberg game";
        if (!Application.isEditor)
        {
            int sdkVersion=23;
            using (var buildVersion = new AndroidJavaClass("android.os.Build$VERSION"))
            {
                sdkVersion = buildVersion.GetStatic<int>("SDK_INT");
            }

            if (sdkVersion < 24)
            {
                AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
                AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
                intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText + gameLink);
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
                intentObject.Call<AndroidJavaObject>("setType", "text/plain");
                AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
                currentActivity.Call("startActivity", intentObject);
            }

            else
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
                AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
                intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText + gameLink);
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
                intentObject.Call<AndroidJavaObject>("setType", "text/plain");
                currentActivity.Call("startActivity", intentObject);
            }
        }

        isProcessing = false;

    }

}