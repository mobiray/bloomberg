﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPrivacy : MonoBehaviour {

    public void OnOpenPressed()
    {
        Application.OpenURL("https://mobiray.github.io/com.twodtwob.trading.game");
    }
}
