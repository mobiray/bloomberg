﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine.UI;
using System;

public class AdvertismentScript : MonoBehaviour, IRewardedVideoAdListener
{

    public GameInfoScript gis;
    public Button watchButton;
    public ChangeSlider cs;
    public EnablingButtons eb;

    static public AdvertismentScript Instance { get { return _instance; } }
    static protected AdvertismentScript _instance;

    void Start()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        string appKey = "d3eedde3b6e7077228f0acc9787b6fbb88267ec860ac0650";
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(appKey, Appodeal.REWARDED_VIDEO | Appodeal.INTERSTITIAL | Appodeal.BANNER_BOTTOM);

        Appodeal.setRewardedVideoCallbacks(this);
        //Appodeal.setTesting(true);
        Appodeal.setLogging(true);
        ShowBanner();

    }

    public bool isRewardedVideoLoaded()
    {
        return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
    }

    public bool isInterstitialLoaded()
    {
        return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
    }

    public bool ShowBanner()
    {
        return Appodeal.show(Appodeal.BANNER_BOTTOM);
    }

    public bool ShowRewardedVideo()
    {
        return Appodeal.show(Appodeal.REWARDED_VIDEO);
    }

    public bool ShowInterstitial()
    {
        bool fl=Appodeal.show(Appodeal.INTERSTITIAL);

        if (fl)
        {
            gis.CurrentAmount += 200;
            gis.SaveAmount();
            cs.MakeChanges();
            eb.MakeButtonsEnabled();
        }
        return fl;
    }

    public void onRewardedVideoLoaded()
    {
       // watchButton.interactable = true;
    }
    public void onRewardedVideoFailedToLoad()
    {
        print("Video failed");
    }
    public void onRewardedVideoShown()
    {
       // watchButton.interactable = false;
        print("Video shown");
    }
    public void onRewardedVideoClosed()
    {
        print("Video closed");
    }
    public void onRewardedVideoFinished(int amount, string name)
    {
        if (null == gis)
        {
            gis = GameObject.Find("GameInformation").GetComponent<GameInfoScript>();
        }
        gis.CurrentAmount +=200;
        gis.SaveAmount();
        cs.MakeChanges();
        eb.MakeButtonsEnabled();
        print("Reward: " + amount + " " + name);
    }

}
