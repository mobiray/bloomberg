﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardedVideo : MonoBehaviour {

	public void OnRewardedVideoClicked()
    {
        try
        {
            if (AdvertismentScript.Instance.isRewardedVideoLoaded())
            {
                Debug.Log("DebugVidLoaded");
                if (!AdvertismentScript.Instance.ShowRewardedVideo())
                    Debug.Log("DebugVidNotShowed");
                Debug.Log("DebugVidLoadedS");
            }
            else
            {
                if (AdvertismentScript.Instance.isInterstitialLoaded())
                {
                    Debug.Log("DebugIntLoaded");
                    if (!AdvertismentScript.Instance.ShowInterstitial())
                        Debug.Log("DebugintNotshowed");
                    Debug.Log("DebugIntLoadedS");
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.StackTrace);
        }
    }

}
