﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace navigation
{
    public class BackToStartScript : MonoBehaviour
    {

        public GameObject parentPanel;
        public GameObject startPanel;

        public void OnBackClick()
        {
            startPanel.SetActive(true);
            parentPanel.SetActive(false);
        }
    }
}