﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace navigation
{
    public class StartButtonScript : MonoBehaviour
    {

        GameObject parentPanel;
        GameObject menuPanel;

        private void Start()
        {
            parentPanel = GameObject.Find("StartPanel");
            menuPanel = GameObject.Find("MenuPanel");
            menuPanel.SetActive(false);
        }

        public void OnStartButtonClick()
        {
            parentPanel.SetActive(false);
            menuPanel.SetActive(true);
        }
    }
}