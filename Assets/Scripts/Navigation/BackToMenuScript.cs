﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace navigation
{
    public class BackToMenuScript : MonoBehaviour
    {

        public GameObject menuPanel;
        public GameObject gameOverPanel;
        public GameObject gamePlay;
        public GameObject inGamePanel;

        public void OnBackToMenuClicked()
        {
            menuPanel.SetActive(true);
            gamePlay.SetActive(false);
            inGamePanel.SetActive(false);
            gameOverPanel.SetActive(false);
        }

    }
}