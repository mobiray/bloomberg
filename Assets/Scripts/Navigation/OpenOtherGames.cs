﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace navigation
{
    public class OpenOtherGames : MonoBehaviour
    {

        public void OnOpenPressed()
        {
            Application.OpenURL("market://search?q=pub:2d2b+Games");
        }
    }
}